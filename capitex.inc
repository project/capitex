<?php

/**
 * @file
 * Capitex
 */

/**
 * Function som tar hand om signalering från Capitex.
 *
 */

function capitex_question() {

  $UserID = addslashes(trim($_GET['UserID']));
  $DataSource = addslashes(trim($_GET['DataSource']));
  $ip = addslashes(trim($_SERVER['REMOTE_ADDR']));

  // Testa om IP är från Capitex!!
  /**
   * Viktigt.. Vi måste få IP som Capitex jobbar med.
   */
  if($ip == '127.0.0.1' || $ip == '212.91.140.76' || $ip == '213.115.65.82'){
      if($UserID AND $DataSource){
        // Testa om markering är redan gjord.
        $capitex_hamta = variable_get('capitex_hamta', 0);

        if($capitex_hamta == 0){
          // Markera att nytt lista finns för hämtning.
          variable_set('capitex_hamta', 1);
          variable_set('capitex_datasource', $DataSource);
          variable_set('capitex_userid', $UserID);

          // Skicka tillbaks svaret OK för Capitex. Meddelandet är mottaget.
          $answer = 'OK';
      }
      else{
          $answer = 'FAIL';
      }
    }
    else{
      // Skicka tillbaks svaret FAIL för Capitex. Meddelandet är ej mottaget.
      // Möjliga orsaker.
      // Fel i kommunikationen.
      // Nedladdnings process hos Eklund är pågående.
      $answer = 'FAIL';
    }

    // Skriv ut ett svar till Capitex.
    $body  = '<html>';
    $body .= '<title>Answer</title>';
    $body .= '<body>';
    $body .= $answer;

    /**
     *  Markera bort denna rad nedan när test är klar.
     */
    $body .= '<br>'. $UserID ." ". $DataSource ." ". $ip;

    $body .= '</body>';
    $body .= '</html>';

    print $body;
  }
}

/**
 * Hämta hem den senaste objekt listan från Capitex.
 *
 */

function import_nyobjektlista(){
  /**
   * Läs in test XML fil tillsvidare.
   */

  # $filename = "/srv/www/vhosts/eklund/web/sites/default/modules/custom/capitex/capitextest/listexempel.xml";
  $filename = "http://objektlista.ws.capitex.se/objects/ObjList.asmx/GetObjects4?LicKey=xxx";
  #$filename = "/home/vhosts/host42/web/sites/default/modules/custom/capitex/capitextest/listexempel.xml";

  if($handle = @fopen($filename, "rb")) {

      $contents = '';

      while (!feof($handle)) {
        $data .= fread($handle, 8192);
      }
      fclose($handle);

      #$data = fread($handle, filesize($filename));
      #fclose($handle);

      $xml_parser = xml_parser_create();

      xml_parse_into_struct($xml_parser, $data, $vals, $index);
      xml_parser_free($xml_parser);

      #$test  = "<pre>";
      #$test .= print_r($index, true);
      #$test .= "<br>";
      #$test .= print_r($vals, true);
      #$test .= "</pre>";

      #watchdog('Capitex', $test, array(), WATCHDOG_NOTICE);

      $objekt = $index['OBJEKT'];

      if(is_array($objekt)){

        $sql = 'TRUNCATE TABLE capitex_importlista';
        db_query($sql);

        foreach($objekt AS $id => $objektid){
          /**
           * Lagra ny lista i MYSQL tabell saop_nylista
           */
          $attributes = $vals[$objektid]['attributes'];
          db_query("INSERT INTO {capitex_importlista} (guid, o_andringsdatum, o_serverandringsdatum, typ) VALUES ('%s', '%s', '%s', '%s')", $attributes['GUID'], $attributes['O_ANDRINGSDATUM'], $attributes['O_SERVERANDRINGSDATUM'], $attributes['TYP']);
          $cnt++;
        }
        watchdog('Capitex', 'Hämtning klar!!', array(), WATCHDOG_NOTICE);
      }
  }
  
  if($cnt > 0){
    return TRUE;
  }
  else{
    return FALSE;
  }
}

function testa_nyobjektlista_motgammal(){
  /**
   * Om ett objekt saknas i den sparade listan men finns i den hämtade då är det ett nytt objekt.
   *
   */
  $result =  db_query("SELECT capitex_importlista.* FROM {capitex_originallista} RIGHT JOIN capitex_importlista ON capitex_originallista.guid = capitex_importlista.guid WHERE capitex_originallista.guid IS NULL");
  /**
   * Spara nya objekt i gamlalistan, sätt flagga ny, uppdaterad.
   */
  while($action = db_fetch_object($result)){
    db_query("INSERT INTO {capitex_originallista} (guid, o_andringsdatum, o_serverandringsdatum, typ, status) VALUES ('%s', '%s', '%s', '%s', " .CAPITEX_NEW. ")", $action->guid, $action->o_andringsdatum, $action->o_serverandringsdatum, $action->typ );
  }
  /**
   * Om ett objekt finns i den sparade listan men saknas i den hämtade  är det ett objekt som har raderats.
   */
  $result = db_query("SELECT capitex_originallista.guid FROM {capitex_importlista} RIGHT JOIN {capitex_originallista} ON capitex_originallista.guid = capitex_importlista.guid WHERE capitex_importlista.guid IS NULL");
  /**
   * Ta bort objekt i original listan (gamla listan)
   */
  while($action = db_fetch_object($result)){
    db_query("UPDATE {capitex_originallista} SET capitex_originallista.status = ". CAPITEX_DELETED. " WHERE capitex_originallista.guid = '%s' ", $action->guid);
    # watchdog('Capitex', "Update capitex_originallista SET capitex_originallista.status = " .capitex_DELETED . " WHERE capitex_originallista.guid = '$action->guid' ", array(), WATCHDOG_NOTICE);
  }
  /**
   * Om ett objekt finns både i den sparade och i den hämtade listan, jämför man ändrings datum för att se om objektets information har förändrats.
   */
  $result = db_query("SELECT capitex_importlista.* FROM {capitex_importlista} RIGHT JOIN {capitex_originallista} ON capitex_originallista.guid = capitex_importlista.guid WHERE capitex_importlista.guid IS NOT NULL AND capitex_importlista.o_andringsdatum <> capitex_originallista.o_andringsdatum");
  /**
   * Uppdatera i original listan datum (gamla listan) Sätt flagga ny, uppdaterad.
   */
  while($action = db_fetch_object($result)){
    db_query("UPDATE {capitex_originallista} SET capitex_originallista.status = ". CAPITEX_UPDATED. ", capitex_originallista.o_andringsdatum = '%s', capitex_originallista.o_serverandringsdatum = '%s', capitex_originallista.typ = '%s' WHERE capitex_originallista.guid = '%s' ", $action->o_andringsdatum, $action->o_serverandringsdatum, $action->typ, $action->guid);
    # watchdog('Capitex', "Update capitex_originallista SET capitex_originallista.status = " .capitex_DELETED . " WHERE capitex_originallista.guid = '$action->guid' ", array(), WATCHDOG_NOTICE);
  }
}
